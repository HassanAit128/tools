#!/usr/bin/env bash

# anaconda show bioconda/snakemake 2>&1 1>/dev/null | grep "Summary:" | cut -f2 -d':' | awk '{$1=$1;print}'

for opt in "$@"
do
case $opt in
    -h|--help)
    echo "add_conda_tool <channel>/<tool>/<version>"
    exit 0
    ;;
esac
done

anaconda="yes"
command -v anaconda >/dev/null 2>&1 || { echo >&2 "anaconda is not available. Generating minimal meta.yml file"; anaconda="no"; }

if awk -F "/" '{exit NF != 3 ? 0 : 1}' <<< "${1}" ; then
  echo >&2 "invalid parameter $1"
  echo >&2 "please specify tool to add with the following syntax <channel>/<tool>/<version>"
  exit 1
fi

channel=$(echo $1 | cut -f1 -d/)
tool=$(echo $1 | cut -f2 -d/)
version=$(echo $1 | cut -f3 -d/)

if [[ "${anaconda}" = "yes" ]]; then
  anaconda show ${channel}/${tool} 2> /tmp/tool_description.txt 1>/dev/null
  # check if tool is available
  if [[ $(wc -l </tmp/tool_description.txt) -eq 1 ]]; then
    echo >&2 "${tool} is not available in ${channel}"
    exit 1
  fi
  # check if version is available
  if ! grep -A 1000 "Versions:" /tmp/tool_description.txt | grep -v "Versions:" | grep ${version} > /dev/null; then
    echo "version ${version} of ${tool} is not available in ${channel}"
    exit 1
  fi

  description=$(grep "Summary:" /tmp/tool_description.txt | cut -f2- -d':' | awk '{$1=$1;print}')
fi

if [ -z "$description" ]; then
  description="<Write a simple description of the tool>"
fi

mkdir -p tools/$tool/$version

# generating meta.yml
eval "cat >tools/$tool/$version/meta.yml <<'EOF'
deployment: conda

about:
  description: \"${description}\"
EOF
" 2> /dev/null

# generating channels list
channels_highpriority='- conda-forge'
channels_lowpriority='- defaults'
channels="${channels_highpriority}\n"
if ! [ "$channel" = "conda-forge" -o "$channel" = "defaults" ]; then
  channels="${channels}- ${channel}\n"
fi
channels="${channels}${channels_lowpriority}\n"

# generating env.yml
eval "cat >tools/$tool/$version/env.yml <<'EOF'
name: ${tool}-${version}
channels:
$(echo -e $channels)
dependencies:
- ${channel}::${tool}=${version}
EOF
"
