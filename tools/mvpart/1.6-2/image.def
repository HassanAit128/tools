Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version 1.6-2

%post
    export PATH=/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive

    # prepare debian/ubuntu
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda

    # install R with conda
    conda update -q -y conda
    conda install -q -y -c conda-forge -y conda-forge::r-base=3.6.1 r-hmisc r-labdsv r-vegan r-fbasics r-ade4
    conda clean -q -y --all

    cd /opt
    wget -q https://cran.r-project.org/src/contrib/Archive/mvpart/mvpart_1.6-2.tar.gz
    R CMD INSTALL mvpart_1.6-2.tar.gz
    wget -q https://cran.r-project.org/src/contrib/Archive/MVPARTwrap/MVPARTwrap_0.1-9.tar.gz
    R CMD INSTALL MVPARTwrap_0.1-9.tar.gz
    rm -rf /opt/*.tar.gz


%environment
    export PATH=/opt/conda/bin:$PATH

%test
    export PATH=/opt/conda/bin:$PATH

    R --vanilla -e 'library(mvpart)'
    R --vanilla -e 'library(MVPARTwrap)'

