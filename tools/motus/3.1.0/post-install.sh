#!/bin/bash

# Set software name and version here
SOFTWARE_NAME="motus"
SOFTWARE_VERSION="3.1.0"

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

if conda activate ${SOFTWARE_NAME}-${SOFTWARE_VERSION}; then
    conda activate ${CONDA_HOME}/envs/${SOFTWARE_NAME}-${SOFTWARE_VERSION}
fi

if [ "${SOFTWARE_NAME}-${SOFTWARE_VERSION}" != "${CONDA_DEFAULT_ENV}" ]; then
    echo "Unable to load ${SOFTWARE_NAME}-${SOFTWARE_VERSION} env"
    exit 1;
fi

set -e

motus downloadDB
