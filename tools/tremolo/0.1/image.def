Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version master

%environment
  export PATH=/opt/TrEMOLO:/opt/conda/bin:$PATH

%post
    export PATH=/opt/TrEMOLO:/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive
    # prepare debian/ubuntu
    apt-get update && apt-get upgrade -y
    apt-get install -y  \
            apt-utils \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common \
            sed \
            wget \
            git \
            build-essential \
            python3 python3-pip
    apt-get clean
    apt-get purge


    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    # install dependency
    conda update -y conda
    . /opt/conda/etc/profile.d/conda.sh
    conda install -c conda-forge -y mamba
    mamba install -y -c bioconda -c conda-forge -c defaults\
          "blast>=2.2" \
          "bedtools>=2" \
          assemblytics \
          drmaa \
          "snakemake>=5.5.2" \
          "minimap2>=2.16" \
          "samtools>=1.10" \
          "sniffles>=1.0.10" \
          tk \
          "r-base>=3.3" \
          r-ggplot2 \
          r-rcolorbrewer \
          r-extrafont \
          "python>=3.6" \
          numpy \
          matplotlib \
          pandas \
          biopython

    # get TrEMOLO
    git clone https://github.com/DrosophilaGenomeEvolution/TrEMOLO.git /opt/TrEMOLO
    chmod +x /opt/TrEMOLO/svTEidentification.py


%runscript
    . /opt/conda/etc/profile.d/conda.sh
    exec svTEidentification.py "$@"

%test
    export PATH=/opt/TrEMOLO:/opt/conda/bin:$PATH
    . /opt/conda/etc/profile.d/conda.sh
    svTEidentification.py -v
    svTEidentification.py -h
