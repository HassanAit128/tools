#!/bin/bash
  
version=1.6
cd ${CONDA_HOME}/envs/platon-${version}/lib/python3.11/site-packages
wget https://zenodo.org/record/4066768/files/db.tar.gz
tar -xvzf db.tar.gz
rm db.tar.gz