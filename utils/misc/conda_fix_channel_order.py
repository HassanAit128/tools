#!/usr/bin/env python3



import os
import sys

from yaml import dump, safe_load

def fix_channel_order(env_yml):
    """
    change the channel order in a env.yml
    add conda-forge on top
    """
    print(env_yml)
    with open(env_yml) as f:
        env = safe_load(f)
    channels = env.get('channels', [])
    channels_new = ['conda-forge']
    for channel in channels:
        if channel == 'conda-forge':
            continue
        channels_new.append(channel)
    env['channels'] = channels_new
    if channels != channels_new:
        with open(env_yml, "w") as f:
            dump(env, f)


if __name__ == '__main__':
    fix_channel_order(sys.argv[1])