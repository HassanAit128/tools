#!/usr/bin/env python3

import os
import os.path
import subprocess
import sys

from string import Template
from yaml import load, Loader

CWD = os.path.abspath(os.getcwd())

def build_wrappers(software, version):
    """Build and deploy wrappers and modulefile."""
    meta_path = os.path.join(CWD, 'tools', software, version, "meta.yml".format(software, version))

    meta = load(open(meta_path, "r"), Loader=Loader)

    common_options = ''
    if 'with_gpu_support' in meta and meta['with_gpu_support'] is True:
        common_options = '--nv'

    binds = meta.get('binds', [])

    binaries = []
    binaries_paths = meta.get('binaries_paths', [])
    for binaries_path in binaries_paths:
        binaries += ls_executable_in_image(software, version, binaries_path)

    if 'binaries' in meta:
        binaries += meta['binaries']

    for binary in binaries:
        options = common_options
        binds = []
        if type(binary) == dict:
            name = list(binary.keys())[0]
            if 'binds' in binary[name]:
                binds = binds + binary[name]['binds']
        else:
            name = binary

        if len(binds) > 0:
            if len(options) > 0:
                options += ' '
            options += '-B '
            options += ','.join(
                ['{src}:{dest}'.format(**bind) for bind in binds])

        build_wrapper(software, version, name, options)

    if 'make_env_wrapper' in meta and meta['make_env_wrapper'] is True:
        build_wrapper(software, version, None, options)

    # template_path = os.path.join(os.path.dirname(__file__), 'modulefile.j2')
    # template_desc = open(template_path, 'r')
    # modulefile_template = JinjaTemplate(template_desc.read())
    #
    # modulefiles_dir = os.path.join(os.environ['MODULEFILES_PATH'], software)
    # os.makedirs(modulefiles_dir, exist_ok=True)
    # modulefile_path = os.path.join(modulefiles_dir, version)
    # wrappers_path = os.path.join(os.environ['CLUSTER_WRAPPERS_PATH'], software, version)
    # description = meta['description']
    # url = meta.get('url', '')
    # modules = meta.get('require', [])
    #
    # modulefile = open(modulefile_path, "w")
    # modulefile.write(modulefile_template.render(
    #     modules=modules,
    #     software=software,
    #     version=version,
    #     url=url,
    #     description=description,
    #     wrappers=wrappers_path))
    # modulefile.close()


def get_image_filename(software, version):
    """Return default image path for software and version."""
    return "{software}-{version}.sif".format(software=software, version=version)


def build_wrapper(software, version, binary, options):
    """Buid and install a wrapper for a given binary."""
    wrapper_template = Template("""#! /usr/bin/env bash
singularity exec $options $image_path $binary $$@""")  #NOQA

    image_file = get_image_filename(software, version)

    wrappers_path = os.path.join(os.environ['SINGULARITY_REPOSITORY_PATH'], 'wrappers', software, version)
    os.makedirs(wrappers_path, exist_ok=True)

    name = binary if binary is not None else 'run_with_{}'.format(software)

    wrapper_path = os.path.join(wrappers_path, name)
    wrapper = open(wrapper_path, "w")
    wrapper.write(wrapper_template.substitute(
        image_path=os.path.join(os.environ['SINGULARITY_REPOSITORY_PATH'], 'images', image_file),
        binary=binary if binary is not None else '',
        options=options))
    wrapper.close()
    os.chmod(wrapper_path, 0o755)


def ls_executable_in_image(software, version, path):
    image_path = os.path.join(os.environ['SINGULARITY_REPOSITORY_PATH'], 'images', get_image_filename(software, version))

    ls = subprocess.run(['singularity', 'exec', image_path, 'find', '-L', path, '-maxdepth', '1', '-type', 'f', '-executable', '-print'],
                        stdout=subprocess.PIPE)
    result = ls.stdout.decode('utf-8')
    files = result.split('\n')
    files = [os.path.basename(file) for file in files[:-1]]
    return files


if __name__ == '__main__':
    build_wrappers(sys.argv[1], sys.argv[2])
