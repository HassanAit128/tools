#!/usr/bin/env bash

set -e

df -h

source ci/utils.sh

function create_module_file {
    export software=$1
    export version=$2
    export wrappers_path=${SINGULARITY_REPOSITORY_PATH}/wrappers/${software}/${version}

    j2 ci/singularity/modulefile.j2 tools/${software}/${version}/meta.yml
}

tools_list=$1

if [ -z $tools_list ]; then
    echo "No tools list provided"
    exit 1
fi

# creating envs
for software_line in $(cat $tools_list); do

    software=$(echo $software_line | cut -f1 -d/);
    version=$(echo $software_line | cut -f2 -d/);

    image_path=${SINGULARITY_REPOSITORY_PATH}/images/${software}-${version}.sif
    definition_path_basename=tools/${software}/${version}/
    definition_filename=image.def

    SINGULARITY_UTILS_CMD="python3 ci/singularity/singularity_utils.py --software ${software} --version ${version} --function "
    if [ $($SINGULARITY_UTILS_CMD is_local_src_needed) -eq 1 ]; then
        if [ $($SINGULARITY_UTILS_CMD is_infra_software_src_path) -eq 0 ]; then
            echo "ABORT: the env variable SOFTWARE_SRC_PATH isn't defined for this infrastucture"
            echo -e "Skipped \xf0\x9f\xa6\x98"
            continue
        fi
        if [ -z "$USE_LOCAL_SOFTWARE_SRC" ]; then
            mkdir -p /src
            sshfs $CLUSTER_USER@$CLUSTER_HOST:$SOFTWARE_SRC_PATH /src
        elif [ $USE_LOCAL_SOFTWARE_SRC -eq 1 ]; then
            ln -s $SOFTWARE_SRC_PATH /src
        fi
        if [ $($SINGULARITY_UTILS_CMD is_local_src_exists) -eq 0 ]; then
            echo "ABORT: some local src files are missing on this infrastucture"
            echo -e "Skipped \xf0\x9f\xa6\x98"
            continue
        fi
    fi

    echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

    echo "Building Singularity image..."
    # cd to the .def directory to allow side files %files
    cd $definition_path_basename
    singularity build --force $image_path $definition_filename
    # this check on $? must be after the command tested
    if [ $? -ne 0 ]; then
        echo "Build failed"
        df -h
        exit 1
    fi

    if [ ! -z "$USE_LOCAL_SOFTWARE_SRC" ] && [ $USE_LOCAL_SOFTWARE_SRC -eq 1 ]; then
        rm /src
    fi

    cd -

    ### Don't build wrappers and modulefiles hook
    if [ -f ${definition_path_basename}/no-deploy.hook ]
    then
        echo -e "\033[1mTool marked as no-deploy (no wrapper or modulefile will be created)\033[0m \xF0\x9F\x9A\x80"
        continue
    fi

    echo "Creating wrappers..."
    python3 ci/singularity/build_wrappers.py $software $version

    echo "Creating modulefile..."
    aliases=$($SINGULARITY_UTILS_CMD get_aliases)
    if [[ "$aliases" == 0 ]]; then
        mkdir -p ${MODULEFILES_PATH}/${software}
        create_module_file $software $version > ${MODULEFILES_PATH}/${software}/${version}
    else
        for alias in $aliases; do
            if [ $(echo $alias |  grep ".*/.*") ]; then
                alias_software=$(echo $alias | cut -f 1 -d '/')
                alias_version=$(echo $alias | cut -f 2 -d '/')
                mkdir -p ${MODULEFILES_PATH}/${alias_software}
                create_module_file $software $version > ${MODULEFILES_PATH}/${alias_software}/${alias_version}
            else
                echo "WARNING: the alias $alias doesn't match .*/.*"
                continue
            fi
        done
    fi

done
